package com.yjy.monster;

public interface DangerousMonster extends Monster {
    public void destroy();
}
