package com.yjy.monster;

public class HorrorShow {
    public static void u(Monster b) {
        b.menace();
    }

    public static void v(DangerousMonster d) {
        d.destroy();
        d.menace();
    }

    public static void w(Lethal b) {
        b.kill();
    }

    public static void main(String[] args) {

        DangerousMonster barney = new DragonZilla();
        u(barney);
        v(barney);
        Vampire vlad = new VeryBadVampire();
        u(vlad);
        v(vlad);
        w(vlad);


    }

}