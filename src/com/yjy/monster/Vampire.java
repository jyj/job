package com.yjy.monster;

public interface Vampire extends DangerousMonster, Lethal {
    public void drinkBlood();
}
