package com.yjy.monster;

public class VeryBadVampire implements Vampire {
    @Override
    public void menace() {
        System.out.println("menace");
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }

    @Override
    public void kill() {
        System.out.println("kill");
    }

    @Override
    public void drinkBlood() {
        System.out.println("drinkBlood");

    }
}
