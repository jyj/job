package com.yjy.toy;

public class ToyTest {
    public static void printInfo(Class cc) {
        // getClass()
        // isInterface() 检查该类型是否是接口
        System.out.println("Class name: " + cc.getClass() + " is interface?[" + cc.isInterface() + "]");
        // getSimpleName()  获取该类型的类型名
        System.out.println("Simple name: " + cc.getSimpleName());
        // getCanonicalName 获取含有包名的该类类型名
        System.out.println("Canonical name: " + cc.getCanonicalName());


    }

    public static void main(String[] args) {
        // Class clazz---->创建一个未知的类的类型，使用getClass获取类型
        Class c = null;
        try {
            // 给c赋一个类型
            c = Class.forName("com.yjy.toy.FancyToy");
        } catch (Exception e) {
            System.out.println("there is no FancyToy");
        }
        if (c != null) {
            printInfo(c);
        }
        // getInterfaces 返回Class 对象中所包含的接口
        for (Class face : c.getInterfaces()) {
            printInfo(face);
        }
        // getSuperclass() 返回c的父类Class
        Class up = c.getSuperclass();
        Object obj = null;
        try {
            // newInstance()实现虚拟构造器
            obj = up.newInstance();


        } catch (InstantiationException e) {
            System.out.println("there is no instantiate");

        } catch (IllegalAccessException e) {
            System.out.println("can't access");
        }
        printInfo(obj.getClass());
        System.out.println(obj.getClass());
    }
}
