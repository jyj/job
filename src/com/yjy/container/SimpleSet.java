package com.yjy.container;

import java.util.*;

public class SimpleSet {
    public static void main(String[] args) {
        Collection<Integer> set = new HashSet<Integer>();
        for (int i = 10; i > 0; i--) {
            set.add(i);
        }
        for (Integer o : set) {
            System.out.println(o);
        }
    }
}
