package com.yjy.container;

import java.util.*;

public class PrintContainers {
    public static Collection<String> fill(Collection<String> collection) {
        collection.add("fish");
        collection.add("dog");
        collection.add("cat");
        collection.add("pig");
        return collection;
    }

    public static Map<String, String> fill(Map<String, String> map) {
        map.put("fish", "swim");
        map.put("cat", "catch");
        map.put("dog", "talk");
        map.put("pig", "eat");
        return map;
    }

    public static void main(String[] args) {
        System.out.println(fill(new ArrayList<>()) + " ArrayList");
        System.out.println(fill(new LinkedList<>()) + " LinkedList");
        System.out.println(fill(new HashSet<>()) + " HashSet");
        System.out.println(fill(new TreeSet<>()) + " TreeSet");
        System.out.println(fill(new HashMap<>()) + " HashMap");
        System.out.println(fill(new TreeMap<>()) + " TreeMap");
        System.out.println(fill(new LinkedHashSet<>()) + " LinkedHashSet");
        System.out.println(fill(new LinkedHashMap<>()) + " LinkedHashMap");


    }


}
