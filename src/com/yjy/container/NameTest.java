package com.yjy.container;

import java.util.HashSet;
import java.util.Set;

public class NameTest {
    public static void main(String[] args) {

        Set<Name> s1 = new HashSet<>();
        s1.add(new Name("小红"));
        s1.add(new Name("小明"));
        s1.add(new Name("小黑"));
        s1.add(new Name("小黄"));
        System.out.println(s1);
    }
}
