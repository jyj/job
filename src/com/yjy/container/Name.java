package com.yjy.container;

public class Name {
    String name;
    int age;

    public Name() {
    }

    public Name(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Name{" +
                "name=" + name +
                '}';
    }

    @Override
    public int hashCode() {
        return super.hashCode();
//        return 0;
    }
}
