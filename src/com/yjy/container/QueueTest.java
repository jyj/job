package com.yjy.container;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class QueueTest {
    public static void queueDemo(Queue queue) {
        while (queue.peek() != null) {
            System.out.println(queue.remove() + " ");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Queue q1 = new LinkedList();
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            q1.offer(random.nextInt(i + 10));
        }
        queueDemo(q1);
        Queue<Character> q2 = new LinkedList<>();

        for (Character o : "Brontosaurus".toCharArray()) {
            q2.offer(o);
        }
        queueDemo(q2);
    }
}
