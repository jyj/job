package com.yjy.container;

import java.util.*;

public class SetOfInteger {
    public static void main(String[] args) {
        Set<Integer> s1 = new HashSet<>();
        for (int i = 0; i < 1000; i++) {
            Random random = new Random();
            Integer integer = random.nextInt(30);
//            System.out.println(integer + ":" + Integer.hashCode(integer));
            s1.add(integer);
        }
        System.out.println(s1);
    }
}
