package com.yjy.container;

import java.util.*;

public class SimpleCollection {
    public static void main(String[] args) {
        List<Integer> c = new ArrayList<Integer>();
        Integer[] integer = new Integer[]{6, 7, 8, 9, 10};

        for (int i = 0; i < 5; i++) {
            c.add(i);
        }
        c.addAll(Arrays.asList(integer));
        for (Integer o : c) {
            System.out.println(o);
        }
        System.out.println(c.contains(8));
        System.out.println(c.isEmpty());
        System.out.println(c.indexOf(0));

    }
}
