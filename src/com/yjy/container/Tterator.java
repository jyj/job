package com.yjy.container;

import java.util.*;

public class Tterator {
    public static Collection<String> fill(Collection<String> collection) {
        collection.add("fish");
        collection.add("dog");
        collection.add("cat");
        collection.add("pig");
        return collection;
    }

    public static Map<String, String> fill(Map<String, String> map) {
        map.put("fish", "swim");
        map.put("cat", "catch");
        map.put("dog", "talk");
        map.put("pig", "eat");
        return map;
    }

    public static void main(String[] args) {
        Collection<String> list = fill(new ArrayList<String>());
        Map<String, String> map = fill(new HashMap<>());
        Iterator<String> iterable1 = list.iterator();
        while (iterable1.hasNext()) {
            System.out.println(iterable1.next());
        }

        Set<Map.Entry<String, String>> set = map.entrySet();
//        System.out.println(map.entrySet());

        Iterator<Map.Entry<String, String>> iterable2 = set.iterator();

        while (iterable2.hasNext()) {
            System.out.println(iterable2.next());
        }
    }
}
