package com.yjy.container;

import java.util.*;

public class MapOfInteger {
    public static void main(String[] args) {
        Map<Integer, Integer> map = new HashMap<>();
        Random random = new Random();
        for (int i = 0; i < 1000; i++) {
            int r = random.nextInt(30);
            Integer in = map.get(r);
            map.put(r, in == null ? 1 : in + 1);
        }

        System.out.println(map);


    }
}
