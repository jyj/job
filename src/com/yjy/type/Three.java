package com.yjy.type;

public class Three<A, B, C> extends TwoTuple<A, B> {
    public final C c;

    public Three(A a, B b, C c) {
        super(a, b);
        this.c = c;
    }

    @Override
    public String toString() {
        return "Three{" +
                "c=" + c +
                ", one=" + one +
                ", two=" + two +
                '}';
    }
}
