package com.yjy.type;

public class Four<A, B, C, D> extends Three<A, B, C> {
    public final D d;

    public Four(A a, B b, C c, D d) {
        super(a, b, c);
        this.d = d;
    }

    @Override
    public String toString() {
        return "Four{" +
                "d=" + d +
                ", c=" + c +
                ", one=" + one +
                ", two=" + two +
                '}';
    }
}
