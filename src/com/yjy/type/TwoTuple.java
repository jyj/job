package com.yjy.type;

public class TwoTuple<A, B> {
    public final A one;
    public final B two;

    public TwoTuple(A a, B b) {
        this.one = a;
        this.two = b;
    }

    @Override
    public String toString() {
        return "TwoTuple{" +
                "one=" + one +
                ", two=" + two +
                '}';
    }
}
