package com.yjy.persontest;

public class GenericPerson {
    public static void main(String[] args) {
        Person<Contact> per = new Person<>(new Contact("北京市", "01088888888", "102206"));
        System.out.println(per);

        Person<Introduction> per2 = new Person<>(new Introduction("李雷", "男", 24));
        System.out.println(per2);
    }
}
