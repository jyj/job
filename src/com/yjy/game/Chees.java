package com.yjy.game;

public class Chees implements Game {
    public static int moves = 0;
    public static final int MOVES = 4;

    @Override
    public boolean move() {
        System.out.println("Chess moves " + moves);
        return ++moves != MOVES;
    }
}
