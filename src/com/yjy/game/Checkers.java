package com.yjy.game;

public class Checkers implements Game {
    public static int moves = 0;
    public static final int MOVES = 3;

    @Override
    public boolean move() {
        System.out.println("Checkers moves " + moves);
        return ++moves != MOVES;
    }

    ;
}
;