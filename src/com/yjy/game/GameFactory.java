package com.yjy.game;

public interface GameFactory {
    public Game getGame();
}
