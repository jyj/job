package com.yjy.typeinformation;

public class GetClassTest {
    public static void main(String[] args) {
        GetClassTest obj = new GetClassTest();
        // 已知一个类，赋给claz
        Class claz = Candy.class;

        // 已知某个实例，获取当前实例的类
        Class<?> clazz = obj.getClass();
        System.out.println(clazz);

        // 输出类的名称
        System.out.println("Class name: " + clazz.getName());

        // 检查类是否是指定类的实例
        if (clazz.equals(GetClassTest.class)) {
            System.out.println("obj is an instance of GetClassTest");
        }

        // 获取类的简单名称（不包括包名）
        System.out.println("Simple class name: " + clazz.getSimpleName());

        // 获取类的包名
        Package pkg = clazz.getPackage();
        if (pkg != null) {
            System.out.println("Package name: " + pkg.getName());
        }

        System.out.println();
        // ... 其他反射操作 ...
    }

}
