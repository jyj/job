package com.yjy.abnormal;

public class TryCatchTest {
    public static void fool() {
        int a = 5 / 0;
        // 不会执行
        System.out.println("look here");

    }

    public static void main(String[] args) {
        try {
            fool();
        } catch (Exception e) {
            System.out.println("error");
            throw new ArithmeticException();

        }
    }
}
