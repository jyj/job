package com.yjy.abnormal;

public class ExceptionMethod {
    public static void main(String[] args) {
        try {
            throw new Exception("my Exception");

        } catch (Exception e) {
            System.out.println("Catch Exception");
            System.out.println("getMessage " + e.getMessage());
            System.out.println("getLocalizedMessage " + e.getLocalizedMessage());
            System.out.println("toString " + e);
            System.out.println("e.printStackTrace() :");
            e.printStackTrace();
        } finally {
            System.out.println("over");
        }


    }
}
