package com.yjy.abnormal;

public class CustomException {

    public static void main(String[] args) {
        A a = new A();
        try {
            a.show(-2);
        } catch (myException e) {
            System.out.println(e.getMessage());
        }
    }
}

class A {
    public void show(int num) throws myException {
        if (num < 0) {
            myException me = new myException("异常：" + num + "不是正数");
            throw me;
            //抛出异常，结束方法show()的执行
        }
        System.out.println(num);
    }
}

