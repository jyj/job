package com.yjy.complexmodels;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;
import java.util.stream.Stream;
import java.util.concurrent.atomic.AtomicInteger;

public class MyPro {

    public static void main(String[] args) {
            AtomicInteger counter = new AtomicInteger(0);
            Stream<Integer> infiniteStream = Stream.generate(counter::incrementAndGet);

            infiniteStream.limit(10).forEach(System.out::println); // 输出：1 到 10
        }
    }

