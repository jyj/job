package com.yjy.complexmodels;

public class TypeTestOne<E> {

    //这不是泛型方法
    public E method00() {
        return null;
    }
    /* 不能声明为静态Static的 */

    //这才是泛型方法 可以声明为静态的Static
    public <E> E method01(E input) {
        return input;
    }

    public static void main(String[] args) {
        TypeTestOne<String> str = new TypeTestOne<>();
        TypeTestOne<Integer> integer = new TypeTestOne<>();
        String s = str.method01("Hello");
        Integer i = integer.method01(100);
        System.out.println(s);//Hello
        System.out.println(i);//100
    }
}


