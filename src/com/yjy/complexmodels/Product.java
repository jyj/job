package com.yjy.complexmodels;

import sun.nio.cs.Surrogate;

import java.util.Random;

public class Product {
    private final int id;
    private double price;
    private String description;

    public Product(int id, double price, String description) {
        this.id = id;
        this.price = price;
        this.description = description;
        System.out.println(toString());
    }


    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", price=" + price +
                ", description='" + description + '\'' +
                '}';
    }


    public double changePrice(int add) {
        price += add;
        return price;
    }


//    public static Generator<Product> generator = new Generator<Product>() {
//        private Random random = new Random(47);
//        public Product next() {
//            return new Product(random.nextInt(),
//                    Math.round(random.nextDouble() * 1000.0) + 0.99, "Test");
//
//        }
//    }

}
