package com.wym.factory;

public class ShapeFactory {
    public static void main(String[] args) {
        Person<String, Integer> p1 = new Person<>("wym", 12);
        System.out.println(p1);
        Person<Boolean, Double> p2 = new Person<>(true, 12.2);
        System.out.println(p2);
    }
}
