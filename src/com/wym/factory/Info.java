package com.wym.factory;

public interface Info<T> {
    T getKey();
}
