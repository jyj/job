package com.wym.Thread;

import java.util.Date;

public class LiftOff implements Runnable {
    protected int countDown = 10;
    private static int taskCount = 0;
    private final int id = taskCount++;

    public LiftOff() {
    }

    public String status() {
        return "#" + id + "(" + (countDown > 0 ? countDown : "LiftOff") + "),";
    }

    public LiftOff(int countDown) {
        this.countDown = countDown;
    }

    @Override
    public void run() {
        while (countDown-- > 0) {
            System.out.println(new Date() + ":" + Thread.currentThread().getName() + ":" + status());

            //给当前运行线程一个提示，告诉Java线程调度器（由JVM实现）当前线程愿意放弃其在CPU上的执行权，
            // 并回到可运行状态（Runnable），以便于其他处于相同优先级的线程有机会获得CPU时间片。
//            Thread.yield();
            try {
                Thread.sleep(10000);
            } catch (Exception e) {
                System.out.println(e);
            }


        }
    }
}
