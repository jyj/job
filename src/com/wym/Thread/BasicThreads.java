package com.wym.Thread;

import java.util.Date;

public class BasicThreads {
    public static void main(String[] args) {

        for (int i = 0; i < 5; i++) {
//            Thread t  = new Thread(new LiftOff());
            new Thread(new LiftOff()).start();
            //new 一个 t1，执行一个start，线程0----->9
            // i++循环
            // new 一个t2，执行一个start，线程1----->9
        }
        // 结果显示五个线程为同时进行
        System.out.println(System.currentTimeMillis() + ":" + Thread.currentThread().getName() + ": Waiting for LiftOff");


    }
}
