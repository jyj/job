package com.wym.windowlock;

import java.util.concurrent.locks.ReentrantLock;

public class Window implements Runnable {
    int ticket = 100;
    //1. 创建Lock的实例，必须确保多个线程共享同一个Lock实例
    private final ReentrantLock lock = new ReentrantLock();

    @Override
    public void run() {

        while (true) {
            try {
                //2. 调动lock()，实现需共享的代码的锁定
                lock.lock();
                if (ticket > 0) {

                    System.out.println(Thread.currentThread().getName() + " " + ticket--);
                } else {
                    break;
                }
            } finally {
                //3. 调用unlock()，释放共享代码的锁定
                lock.unlock();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}


