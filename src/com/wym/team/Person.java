package com.wym.team;

public class Person extends Data {

    public static void main(String[] args) {
        Person person = new Person();
        Boy boy = new Boy();
        System.out.println(toString(person));
        System.out.println(toString(boy));
    }

    public static <T extends Data> Data toString(T object) {
        Data data = new Data();
        data.setData(object.toString());
        return data;
    }
}
