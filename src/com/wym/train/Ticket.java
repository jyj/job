package com.wym.train;

public class Ticket {
    private int ticket = 100;

    public void sale() {
        if (ticket > 0) {
            try {
                //加入这个，使得问题暴露的更明显
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + "卖出一张票，票号:" + ticket);
            ticket--;
        } else {
            throw new RuntimeException("没有票了");
        }
    }

    public int getTicket() {
        return ticket;
    }
}
