package com.wym.train;

public class TicketWindow extends Thread {
    private int ticket = 100;

    @Override
    public void run() {
        while (ticket > 0) {
            System.out.println(getName() + "卖出一张票，票号:" + ticket);
            ticket--;
        }
    }
}
