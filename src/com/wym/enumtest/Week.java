package com.wym.enumtest;

public enum Week {
    MONDAY("星期一", 1),
    TUESDAY("星期二", 2),
    WEDNESDAY("星期三", 3),
    THURSDAY("星期四", 4),
    FRIDAY("星期五", 5),
    SATURDAY("星期六", 6),
    SUNDAY("星期日", 7);

    private final String description;
    private final int day;


    private Week(String description, int day) {
        this.description = description;
        this.day = day;
    }

    public String getDescription() {
        return description;
    }

    public int getDay() {
        return day;
    }

    @Override
    public String toString() {
        return super.toString() + ":" + description + " " + day;
    }
}
