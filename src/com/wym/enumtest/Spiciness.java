package com.wym.enumtest;

public enum Spiciness {
    NOT, MILE, MEDIUM, HOT, FLAMING
}
