package com.wym.refcetTest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

public class StudentTest {
    public static void main(String[] args) throws Exception {
        Class clazz = Class.forName("com.wym.refcetTest.Student");
        Field f1 = clazz.getDeclaredField("age");
        f1.setAccessible(true);
        Object stu = clazz.newInstance();
        // 获取构造器对象
//        Constructor<?> constructor = clazz.getDeclaredConstructor(String.class, int.class);
//        Object stu = constructor.newInstance(8, "hh");
        Object g1 = f1.get(stu);
        System.out.println(g1);
        f1.set(stu, 2);
        g1 = f1.get(stu);
        System.out.println(g1);

    }
}
