package com.wym.compareTest;

import java.util.Comparator;

public class TreeComparatorTest implements Comparator<Tree> {

    @Override
    public int compare(Tree o1, Tree o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
