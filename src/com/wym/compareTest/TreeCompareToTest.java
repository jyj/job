package com.wym.compareTest;

import java.util.HashSet;
import java.util.Set;

public class TreeCompareToTest {
    public static void main(String[] args) {
        Tree t1 = new Tree(18, "wym");
        Tree t2 = new Tree(16, "yjy");

        Tree t3 = new Tree(18, "wym");
        System.out.println(t1.compareTo(t2));
        System.out.println(t1.compareTo(t3));
        Set<Tree> tt = new HashSet<>();
        tt.add(t1);
        tt.add(t2);
        tt.add(t3);
        for (Tree o : tt) {
            System.out.println(o.getAge());
        }


    }
}
