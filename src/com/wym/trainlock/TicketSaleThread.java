package com.wym.trainlock;

public class TicketSaleThread extends Thread {
    private static int ticket = 100;

    @Override
    public void run() {//直接锁这里，肯定不行，会导致，只有一个窗口卖票
        while (ticket > 0) {
            saleOneTicket();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {

            }
        }
    }

    public synchronized static void saleOneTicket() {
        //锁对象是TicketSaleThread类的Class对象，而一个类的Class对象在内存中肯定只有一个
        if (ticket > 0) {
            //不加条件，相当于条件判断没有进入锁管控，线程安全问题就没有解决
            System.out.println(Thread.currentThread().getName() + "卖出一张票，票号:" + ticket);
            ticket--;
        }
    }
}
