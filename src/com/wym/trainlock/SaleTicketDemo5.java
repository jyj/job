package com.wym.trainlock;

public class SaleTicketDemo5 {
    public static void main(String[] args) {
        //2、创建资源对象
        Ticket ticket = new Ticket();
        //3、启动多个线程操作资源类的对象
        Thread t2 = new Thread("窗口二") {
            @Override
            public void run() {
                while (true) {
                    synchronized (ticket) {
                        ticket.sale();
                    }
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {

                    }
                }
            }
        };


        Thread t1 = new Thread("窗口一") {
            //不能给run()直接加锁，因为t1,t2,t3的三个run方法分别属于三个Thread类对象，
            @Override
            public void run() {
                // run方法是非静态方法，那么锁对象默认选this，那么锁对象根本不是同一个
                while (true) {
                    synchronized (ticket) {
                        ticket.sale();
                    }
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {

                    }

                }
            }
        };

        Thread t3 = new Thread("窗口三") {
            @Override
            public void run() {
                while (true) {
                    synchronized (ticket) {
                        ticket.sale();
                    }
                    try {
                        Thread.sleep(500);
                    } catch (Exception e) {

                    }
                }
            }
        };

        t2.start();
        t1.start();
        t3.start();
    }
}
