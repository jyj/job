package com.wym.array;

import java.util.Random;

public class UseGenerator {
    private static Random r = new Random();

    public static class Int implements Generator<Integer> {
        private int mod = 1000;

        public Int() {
        }

        public Int(int modulo) {
            mod = modulo;
        }

        @Override
        public Integer next() {
            return r.nextInt(mod);
        }
    }


    public static void main(String[] args) {
        Int i = new Int();
        System.out.println(i.next());

    }
}
