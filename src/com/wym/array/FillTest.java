package com.wym.array;

import java.util.*;

public class FillTest {
    public static void main(String[] args) {
        int size = 6;
        String[] str = new String[size];
        String[] s1 = new String[5];
        s1[0] = "gg";
        s1[1] = "ff";
        System.out.println(Arrays.toString(s1));
        Arrays.fill(str, "wym");
        Arrays.fill(s1, "wym");
        System.out.println(Arrays.toString(str));
        System.out.println(Arrays.toString(s1));
        Collection<String> coll = new ArrayList();
        coll.add("小李广");
        coll.add("扫地僧");
        coll.add("石破天");
        System.out.println(coll);
        Collection<String> c1 = new ArrayList();
        c1.add("aa");
        c1.add("bb");
        c1.add("cc");
//        coll.add(c1);
        coll.addAll(c1);
        System.out.println(coll);
        System.out.println();


    }
}
