package com.wym.array;

public interface Generator<T> {
     T next();
}
