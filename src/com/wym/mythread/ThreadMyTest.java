package com.wym.mythread;

public class ThreadMyTest {
    public static void main(String[] args) {
        ThreadMy t1 = new ThreadMy();
        ThreadMy t2 = new ThreadMy("线程二");
        t1.start();
        try {
            //使当前正在执行的线程以指定的毫秒数暂停
            Thread.sleep(5000);
        } catch (Exception e) {

        }
        t2.start();
        for (int i = 0; i < 10; i++) {
            System.out.println("main线程！" + i);
        }
    }
}
