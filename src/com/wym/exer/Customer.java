package com.wym.exer;

public class Customer extends Thread {
    private Product p;

    public Customer(Product p) {
        this.p = p;
    }

    @Override
    public void run() {
        System.out.println("消费者开始购买产品");
        while (true) {
            try {
                Thread.sleep(90);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            p.deduceProduct();
        }
    }

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }
}
