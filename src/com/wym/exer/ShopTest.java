package com.wym.exer;

public class ShopTest {
    public static void main(String[] args) {
        Product product = new Product();
        Productor p1 = new Productor(product);
        Customer c1 = new Customer(product);
        Customer c2 = new Customer(product);
        p1.setName("商家");
        c1.setName("顾客一");
        c2.setName("顾客二");

        p1.start();
        c1.start();
        c2.start();


    }
}
