package com.wym.exer;

public class Productor extends Thread {
    private Product p;

    public Productor(Product p) {
        this.p = p;
    }

    @Override
    public void run() {
        System.out.println("商家开始生产产品");
        while (true) {

            try {
                Thread.sleep(40);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            p.addProduct();

        }
    }

    public Product getP() {
        return p;
    }

    public void setP(Product p) {
        this.p = p;
    }
}
