package com.wym.exer;

public class Product {
    private int number;
    private final static int MAX = 20;
    private final static int MIN = 1;

    public synchronized void addProduct() {
        if (number < MAX) {
            number++;
            System.out.println(Thread.currentThread().getName() + "已经生产" + number + "件产品");
            notifyAll();
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }
    }

    public synchronized void deduceProduct() {
        if (number > MIN) {
            number--;
            System.out.println(Thread.currentThread().getName() + "已经消耗" + number + "件产品");
            notifyAll();
        } else {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }


}
