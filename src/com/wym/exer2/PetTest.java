package com.wym.exer2;

import java.util.Arrays;

public class PetTest {

    public static Pet[] goBackArray(int n) {
        Pet[] pets = new Pet[n];
        for (int i = 0; i < n; i++) {
            pets[i] = new Pet();
        }

        return pets;
    }


    public static void main(String[] args) {
        System.out.println(Arrays.toString(goBackArray(5)));
    }
}
